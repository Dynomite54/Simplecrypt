from hashlib import md5
from Crypto.Cipher import AES
from Crypto import Random
from sys import exit
def derive_key_and_iv(password, salt, key_length, iv_length):
    d = d_i = ''
    while len(d) < key_length + iv_length:
        d_i = md5(d_i + password + salt).digest()
        d += d_i
    return d[:key_length], d[key_length:key_length+iv_length]
def encrypt(in_file, out_file, password, key_length=32):
    bs = AES.block_size
    salt = Random.new().read(bs - len('Salted__'))
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    out_file.write('Salted__' + salt)
    finished = False
    while not finished:
        chunk = in_file.read(1024 * bs)
        if len(chunk) == 0 or len(chunk) % bs != 0:
            padding_length = (bs - len(chunk) % bs) or bs
            chunk += padding_length * chr(padding_length)
            finished = True
        out_file.write(cipher.encrypt(chunk))
def decrypt(in_file, out_file, password, key_length=32):
    bs = AES.block_size
    salt = in_file.read(bs)[len('Salted__'):]
    key, iv = derive_key_and_iv(password, salt, key_length, bs)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    next_chunk = ''
    finished = False
    while not finished:
        chunk, next_chunk = next_chunk, cipher.decrypt(in_file.read(1024 * bs))
        if len(next_chunk) == 0:
            padding_length = ord(chunk[-1])
            chunk = chunk[:-padding_length]
            finished = True
        out_file.write(chunk)
killmelist = ['exit', 'exit()', 'quit', 'quit()', 'killme', 'die', 'kill']
def killme(x):
    if str(x) in killmelist:
        print('dying...')
        exit()
    elif str(x) == 'help':
        print('This is a pretty self explanatory script.')
        print('Here are you killme words:')
        for r in killmelist:
            print(r)
while True:
    print('For a list of killme words, enter "help".')
    print('Enter a killme word to quit.')
    whatchuwant = raw_input('Encrypt or Decrypt?:\n')
    killme(whatchuwant)
    if whatchuwant.lower() == 'encrypt':
        in_filename = raw_input('What file to encrpyt?\n')
        killme(in_filename)
        password = raw_input('Password:\n')
        killme(password)
        out_filename = str(in_filename + '.encrypted')
        with open(in_filename, 'rb') as in_file, open(out_filename, 'wb') as out_file:
            encrypt(in_file, out_file, password)
    elif whatchuwant.lower() == 'decrypt':
        in_filename = raw_input('What file to decrpyt?\n')
        killme(in_filename)
        password = raw_input('Password:\n')
        killme(password)
        out_filename = in_filename[:(len(in_filename) - 10)]
        with open(in_filename, 'rb') as in_file, open(out_filename, 'wb') as out_file:
            decrypt(in_file, out_file, password)
    else:
        print('None of that now...')
